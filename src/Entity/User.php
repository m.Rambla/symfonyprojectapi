<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\UserRepository;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @UniqueEntity(
  * fields={"email"},
  * errorPath="email",
  * message="utilisateur existe déja"
  *)
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\NotBlank(message ="email est obligatoire")
     * @Assert\Email(
     *     message = "Email invalide"
     * )
     */
    private $email;

    /**
     *  @var string The hashed password
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message ="password est obligatoire")
     * @Assert\Length(min=6, minMessage="Le mot-de-passe doit faire mini 6 caractères")
     * @Assert\Regex(pattern="/.{0,}[a-z].{0,}/",message="M-d-p doit contenir une minuscule")
     * @Assert\Regex(pattern="/.{0,}[A-Z].{0,}/",message="M-d-p doit contenir une majuscule")
     * @Assert\Regex(pattern="/.{0,}[0-9].{0,}/",message="M-d-p doit contenir un chiffre")
     * @Assert\Regex(pattern="/.{0,}[@&$].{0,}/",message="M-d-p doit contenir un caractère spécial (@&$)")
     */
    private $password;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getRoles()
    {
        return [];
    }

    public function getSalt()
    {
        return '';
    }

    public function getUsername()
    {
        return $this->email;
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }
}
