<?php

namespace App\Controller\Api;


use App\Entity\User;
use App\Form\RegistrationFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RegisterController extends AbstractController
{
    private $userPasswordEncoder;

    public function __construct(UserPasswordEncoderInterface $userPasswordEncoder)
    {
        $this->userPasswordEncoder = $userPasswordEncoder;
    }

    /**
     * @Route("/register", name="api_register")
     */
    public function register(Request $request, UserPasswordEncoderInterface $encoder, EntityManagerInterface $em, ValidatorInterface $validator): JsonResponse
    {
        // Validation
        $erreurs = [];
        $json = $request->getContent();
        $objet = json_decode($json);
        $mdp1 = $objet->password;
        $mdp2 = $objet->password2;
        $email = $objet->username;
        if( $mdp1!=$mdp2 ){
            $erreurs[] = "Les mots de passe doivent être identiques";
        }
        $user = new User();
        $user->setPassword( $mdp1 );
        $user->setEmail($email);
        $user->setPassword($mdp1);
        $errors = $validator->validate($user);
        if( count($errors)>0 ){
            foreach ($errors as $error){
                $erreurs[] = $error->getMessage();
            }
        }

        if( count($erreurs)>0 ){
            $msg = implode("\n", $erreurs);
            return $this->json( ['status'=>500,'message'=>$msg] );
        }

        // Inscription en BD
        $user->setPassword( $encoder->encodePassword( $user , $mdp1) );
        $em->persist($user);
        $em->flush();

        return $this->json( ['status'=>200,'message'=>'Inscription réussie'] );
    }
}
