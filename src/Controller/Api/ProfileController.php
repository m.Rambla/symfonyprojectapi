<?php

namespace App\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/profil")
 */
class ProfileController extends AbstractController
{
    /**
     * @Route("/", name="api_profile")
     */
    public function index(): Response
    {
        $user = $this->getUser();

        return $this->json($user);
    }
}
